//
//  SegmentDisplayTableViewCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 13/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class SegmentDisplayTableViewCell: UITableViewCell
{
   
    @IBOutlet weak var btnNumber1: UIButton!
    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var lblComponentName: UILabel!
    @IBOutlet weak var lblPortName: UILabel!
    @IBOutlet weak var btnNumber2: UIButton!
    @IBOutlet weak var btnNumber3: UIButton!
    @IBOutlet weak var btnNumber4: UIButton!
    @IBOutlet weak var btnNumber5: UIButton!
    @IBOutlet weak var btnNumber6: UIButton!
    @IBOutlet weak var btnNumber7: UIButton!
    @IBOutlet weak var btnNumber8: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    @IBAction func switchStateChaged(sender: UISwitch)
    {
        lblPortName.enabled = switchView.on
      //  lblComponentName.enabled = !lblComponentName.enabled
        btnNumber1.enabled = switchView.on
        btnNumber2.enabled = switchView.on
        btnNumber3.enabled = switchView.on
        btnNumber4.enabled = switchView.on
        btnNumber5.enabled = switchView.on
        btnNumber6.enabled = switchView.on
        btnNumber7.enabled = switchView.on
        btnNumber8.enabled = switchView.on
        
    }
    
    @IBAction func handleBtnPressed(sender: UIButton) {
        
        print("pressed\(sender.tag)")
        
        sender.selected = !sender.selected
    }
}