//
//  SettingsView.swift
//  BiSoft
//
//  Created by Devesh Rawat on 07/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class SettingsView: UIView, UITableViewDelegate, UITableViewDataSource, InBetweenDelegate, SetFlagsDelegate
{
    
    @IBOutlet weak var viewImageViewBG: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    
    var isVisible = false
    var arrComponents = [Int]()
    var arrFlags = [Int]()
    var delegate: ShowHideSettigsViewDelegate!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "SliderTableViewCell", bundle: nil) ,forCellReuseIdentifier: "SliderTableViewCell")
        tableView.registerNib(UINib(nibName: "PickerTableViewCell", bundle: nil) ,forCellReuseIdentifier: "PickerTableViewCell")
         tableView.registerNib(UINib(nibName: "OutputComponentTableViewCell", bundle: nil) ,forCellReuseIdentifier: "OutputComponentTableViewCell")
         tableView.registerNib(UINib(nibName: "SegmentDisplayTableViewCell", bundle: nil) ,forCellReuseIdentifier: "SegmentDisplayTableViewCell")
        tableView.registerNib(UINib(nibName: "IFTableViewCell", bundle: nil) ,forCellReuseIdentifier: "IFTableViewCell")
        tableView.registerNib(UINib(nibName: "StartTableViewCell", bundle: nil) ,forCellReuseIdentifier: "StartTableViewCell")
        tableView.registerNib(UINib(nibName: "FlagTableViewCell", bundle: nil) ,forCellReuseIdentifier: "FlagTableViewCell")
       
        
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0)
    }
//    func changeHeightForCell(count: Int, isVisible: Bool)
//    {
//       isdropDownVisible = isVisible
//        var indexPath = NSIndexPath(forRow: 0, inSection: 0)
//        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//    }
    
    
    @IBAction func btnHideShowAction(sender: AnyObject) {
        
        delegate.handleShowHideAction()
        
    }

    
    func manageExtraCell(inBetweenPressed: Bool) {
        
        if inBetweenPressed
        {
          if arrComponents.count == 2
          {
           arrComponents.append(17)
           tableView.reloadData()
          }
        }
        else
        {
            if arrComponents.count == 3
            {
                arrComponents.removeLast()
                tableView.reloadData()
            }
        }
    }
    
    func refreshData()
    {
      //  tableView.reloadData()
    }
//    func setTheFlags(flag: Int) {
//        arrFlags.append(flag)
//    }
    
    func checkForFlags(){
        
        
        for i in arrComponents
        {
            if i == 6
            {
                arrComponents.appendContentsOf(arrFlags)
                break
            }
            
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        checkForFlags()
        
        return arrComponents.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
       
        
        if arrComponents[indexPath.row] >= 0 && arrComponents[indexPath.row] <= 5
        {

            switch arrComponents[indexPath.row]
            {
                
                
            // These cases are for Wait Button
                
                
              case 0:
                
                let cell: PickerTableViewCell = tableView.dequeueReusableCellWithIdentifier("PickerTableViewCell")! as! PickerTableViewCell
               //  cell.refreshValue()
                 cell.backgroundColor = UIColor.clearColor()
                
                return cell
              
            case 1:
                
                let cell: SliderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SliderTableViewCell")! as! SliderTableViewCell
                cell.lblDuration.text = "Miliseconds"
                cell.viewSlider.maximumValue = 950
                cell.viewSlider.minimumValue = 0
                cell.viewSlider.value = 0
                cell.lblSliderValue.text = "0"
                cell.backgroundColor = UIColor.clearColor()
                return cell
             
            case 2:
                
                let cell: SliderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SliderTableViewCell")! as! SliderTableViewCell
                cell.lblDuration.text = "Seconds"
                cell.viewSlider.maximumValue = 59
                cell.viewSlider.minimumValue = 0
                cell.viewSlider.value = 0
                cell.lblSliderValue.text = "0"
                 cell.backgroundColor = UIColor.clearColor()
                return cell
            
            case 3:
                
                let cell: SliderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SliderTableViewCell")! as! SliderTableViewCell
                cell.lblDuration.text = "Minutes"
                cell.viewSlider.maximumValue = 59
                cell.viewSlider.minimumValue = 0
                cell.viewSlider.value = 0
                cell.lblSliderValue.text = "0"
                 cell.backgroundColor = UIColor.clearColor()
                return cell
                
                
            case 4:
                
                let cell: SliderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SliderTableViewCell")! as! SliderTableViewCell
                cell.lblDuration.text = "Hours"
                cell.viewSlider.maximumValue = 23
                cell.viewSlider.minimumValue = 0
                cell.viewSlider.value = 0
                cell.lblSliderValue.text = "0"
                 cell.backgroundColor = UIColor.clearColor()
                return cell
                
              default:
                print("Invalid Choice")
                
            }
        }
            
        else if arrComponents[indexPath.row] > 5 && arrComponents[indexPath.row] <= 40
        {
            switch arrComponents[indexPath.row]
            {
                
                
       // These cases are for output button
              
            
            case 6:
                
                let cell: OutputComponentTableViewCell = tableView.dequeueReusableCellWithIdentifier("OutputComponentTableViewCell") as! OutputComponentTableViewCell
                cell.lblBiBoxPort.text = "A1"
                cell.lblOutputComponent.text = "BEEPER"
                cell.userInteractionEnabled = true
                cell.viewSliderView.value = 0
                cell.lblSliderValue.text = "0"
                cell.viewSliderView.maximumValue = 65535
                cell.viewSliderView.minimumValue = 0
                 cell.backgroundColor = UIColor.clearColor()
                return cell

             case 7:
                
                let cell: OutputComponentTableViewCell = tableView.dequeueReusableCellWithIdentifier("OutputComponentTableViewCell") as! OutputComponentTableViewCell
                cell.lblBiBoxPort.text = "A1"
                cell.lblOutputComponent.text = "LASER"
                cell.userInteractionEnabled = true
                cell.viewSliderView.value = 0
                cell.lblSliderValue.text = "0"
                cell.viewSliderView.maximumValue = 65535
                cell.viewSliderView.minimumValue = 0
                cell.backgroundColor = UIColor.clearColor()
                return cell
 
            case 8:
                
                let cell: OutputComponentTableViewCell = tableView.dequeueReusableCellWithIdentifier("OutputComponentTableViewCell") as! OutputComponentTableViewCell
                cell.lblBiBoxPort.text = "A1"
                cell.lblOutputComponent.text = "LED"
                cell.userInteractionEnabled = true
                cell.viewSliderView.value = 0
                cell.lblSliderValue.text = "0"
                cell.viewSliderView.maximumValue = 20
                cell.viewSliderView.minimumValue = 0
                cell.backgroundColor = UIColor.clearColor()
                return cell

                
              case 15:
                
                let cell: SegmentDisplayTableViewCell = tableView.dequeueReusableCellWithIdentifier("SegmentDisplayTableViewCell")! as! SegmentDisplayTableViewCell
                 cell.lblComponentName.text = "7Segment Display"
                 cell.lblPortName.text = "-B/C"
                 cell.userInteractionEnabled = true
                 cell.backgroundColor = UIColor.clearColor()
                 return cell

                
       // These cases are for IF Button
            case 16:
                
                let cell: IFTableViewCell = tableView.dequeueReusableCellWithIdentifier("IFTableViewCell")! as! IFTableViewCell
             
//                cell.delegate = self
                
                cell.userInteractionEnabled = true
//                buttonFrame = cell.convertRect(cell.btnComponents, toView: tableView)
                 cell.backgroundColor = UIColor.clearColor()
                cell.delegate = self
                return cell
                
            case 17:
                
                let cell: SliderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SliderTableViewCell")! as! SliderTableViewCell
                cell.lblDuration.hidden = true
                cell.viewSlider.maximumValue = 65535
                cell.viewSlider.minimumValue = 0
                cell.viewSlider.value = 0
                cell.lblSliderValue.text = "0"
                cell.backgroundColor = UIColor.clearColor()
                
               
                return cell

                
                
                
  // these cases are for start button
                
                
            case 18:
                
                 let cell: StartTableViewCell = tableView.dequeueReusableCellWithIdentifier("StartTableViewCell")! as! StartTableViewCell
                 cell.backgroundColor = UIColor.clearColor()
                 cell.delegate = self
                return cell
                
                
            case 31:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BICOUNTER 1"
               // cell.delegate = self
                return cell
                
            case 32:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BICOUNTER 2"
               // cell.delegate = self
                return cell
    
            case 33:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BICOUNTER 3"
              //  cell.delegate = self
                return cell
              
            case 34:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BIFLAG 1"
              //  cell.delegate = self
                return cell
                
            case 35:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BIFLAG 2"
             //   cell.delegate = self
                return cell
                
            case 36:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BIFLAG 3"
              //  cell.delegate = self
                return cell
    
     
            case 37:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BI DATA 1"
              //  cell.delegate = self
                return cell
                
            case 38:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BI DATA 2"
              //  cell.delegate = self
                return cell
                
            case 39:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "BI DATA 3"
             //   cell.delegate = self
                return cell
    
            case 40:
                
                let cell: FlagTableViewCell = tableView.dequeueReusableCellWithIdentifier("FlagTableViewCell") as! FlagTableViewCell
                cell.backgroundColor = UIColor.clearColor()
                cell.lblFlagName.text = "Bluetooth MP3"
               // cell.delegate = self
                return cell
                
            default:
                print("Invalid Option")
    
            }
            
        }
        
        return UITableViewCell()
    }
        
        
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
//        if arrComponents[indexPath.row] == 18
//        {
//            return 320
//        }

        switch arrComponents[indexPath.row] {
            
        case 18:
            return 320
            
        case 15:
            return 96
            
        default:
            return 70
        }
        

    }

}
