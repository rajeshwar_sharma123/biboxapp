//
//  CustomCell.swift
//  Bisoft1
//
//  Created by Bhagat Solanki on 19/10/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell
{

    
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
