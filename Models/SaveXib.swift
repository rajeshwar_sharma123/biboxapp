//
//  SaveXib.swift
//  Bisoft1
//
//  Created by Bhagat Solanki on 19/10/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit


class SaveXib: UIView, UITableViewDelegate, UITableViewDataSource
{
    
    
    
    @IBOutlet weak var tableViewMenu: UITableView!
    var isNewProject : Bool = false
    
    var tableData: [String] = ["Save","SaveAs","Open"]
    
    weak var controllerBase: UIViewController?
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        tableViewMenu.delegate = self
        tableViewMenu.dataSource = self
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        self.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        tableViewMenu.registerNib(UINib(nibName: "CustomCell", bundle: nil) ,forCellReuseIdentifier: "Cell")
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return self.tableData.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! CustomCell
        
        cell.lblName.text = tableData[indexPath.row]
        cell.backgroundColor = UIColor.clearColor()
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let deviceType = UIDevice.currentDevice().model
        
        switch (indexPath.row)
        {
        case 0:
            if(isNewProject){
                if deviceType == "iPad"
                {
                    let mainStoryboard = UIStoryboard(name: "Main2", bundle: NSBundle.mainBundle())
                    let vc : SaveProjectViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SaveProjectViewController") as! SaveProjectViewController
                    controllerBase?.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : SaveProjectViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SaveProjectViewController") as! SaveProjectViewController
                    controllerBase?.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }
            else
            {
                /*Find the path of plist file of project lists in document directory*/
                let fileManager = NSFileManager.defaultManager()
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
                
                let path = documentDirectory.stringByAppendingString("/ProjectList.plist")
                
                /*instantiate a mutable dictionary to copy the contents of the file*/
                var projectList = NSMutableDictionary()
                
                /*Get the id of current selected project*/
                let currentProjectID = appDelegate.currentProj.valueForKey("ID") as! String
                
                /*copy the file in the instantiated dictionary*/
                if(fileManager.fileExistsAtPath(path))
                {
                    projectList = NSDictionary(contentsOfFile: path)?.mutableCopy() as! NSMutableDictionary
                }
                
                /*set the modified selected project corresponding to the selected project ID*/
                projectList.setObject(appDelegate.currentProj, forKey:currentProjectID)
                
                /*write the modified project list in the original file*/
                let isWritten = projectList.writeToFile(path, atomically: true)
                
                
                let alert = UIAlertController(title: "", message: "Project saved successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                controllerBase!.presentViewController(alert, animated: true, completion:nil)
                print("is the file created: \(isWritten)")
                
                
            }
            break
            
        case 1:
            
            if deviceType == "iPad"
            {
                let mainStoryboard = UIStoryboard(name: "Main2", bundle: NSBundle.mainBundle())
                let vc : SaveProjectViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SaveProjectViewController") as! SaveProjectViewController
                controllerBase?.navigationController?.pushViewController(vc, animated: true)
                
            }
            else
            {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc : SaveProjectViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SaveProjectViewController") as! SaveProjectViewController
                controllerBase?.navigationController?.pushViewController(vc, animated: true)
                
            }
            break
            
        case 2:
            
            //            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            //            let vc : ProjectsViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ProjectsViewController") as! ProjectsViewController
            //            controllerBase?.navigationController?.pushViewController(vc, animated: true)
            let controllers:  [UIViewController] = controllerBase!.navigationController!.viewControllers as [UIViewController]
            
            for vc in controllers
            {
                if vc.isKindOfClass(ProjectsViewController)
                {
                    controllerBase?.navigationController?.popToViewController(vc, animated: true)
                    break
                }
            }
           
            
        default:
            break
        }
        
        self.removeFromSuperview()
    }
    
    @IBAction func handleBtnAction(sender: UIButton)
    {
        
        self.removeFromSuperview()
    }
    
    
}
