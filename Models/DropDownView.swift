//
//  DropDownView.swift
//  BiSoft
//
//  Created by Devesh Rawat on 18/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class DropDownView: UIView, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var tableView: UITableView!
    var delegate: DropDownDelegate!
   
    
    @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var arrIFComponents = [String]()
    var arrConditions = [String]()
    var id = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "DropDownTableViewCell", bundle: nil) ,forCellReuseIdentifier: "DropDownTableViewCell")
        
        self.arrConditions = ["GREATER THAN", "LESS THAN", "EQUAL TO", "NOT EQUAL TO", "IN BETWEEN"]
        self.arrIFComponents = ["IR Remote->IR", "BT Slider", "BT Remote"]
    }
    
    @IBAction func btnActionHideView(sender: AnyObject)
    {
        self.removeFromSuperview()
    }

       func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if id == 1
        {
            return arrIFComponents.count
        }
        else if id == 2
        {
            return arrConditions.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: DropDownTableViewCell = tableView.dequeueReusableCellWithIdentifier("DropDownTableViewCell", forIndexPath: indexPath) as! DropDownTableViewCell
        
        cell.textLabel?.textColor = UIColor.blackColor()
        cell.backgroundColor = UIColor.clearColor()
        
        if id == 1
        {
            cell.lblName.text = arrIFComponents[indexPath.row]
        }
        else
        {
            cell.lblName.text = arrConditions[indexPath.row]
        }
        
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(tableView.frame)
        
        let cell: DropDownTableViewCell = tableView.cellForRowAtIndexPath(indexPath)! as! DropDownTableViewCell
        
        cell.selected = false
        
        delegate.updateValueOfButton((cell.lblName.text)!)
        self.removeFromSuperview()
        
    }
    
}
