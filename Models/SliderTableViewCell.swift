//
//  SliderView.swift
//  BiSoft
//
//  Created by Devesh Rawat on 07/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell
{

    override func awakeFromNib()
    {
        super.awakeFromNib()
        lblSliderValue.text = "0"
        
    }
    
    @IBOutlet weak var viewSlider: UISlider!
    
    @IBOutlet weak var lblSliderValue: UILabel!
    
    @IBOutlet weak var lblDuration: UILabel!
    
    
    @IBAction func sliderAction(sender: UISlider) {
        
        
        lblSliderValue.text = String(Int(sender.value))
        
    }
    
    
}
