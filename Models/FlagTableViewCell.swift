//
//  FlagTableViewCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 25/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class FlagTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var lblFlagName: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var delegate: SetFlagsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        segmentControl.enabled = false
        
    }
    @IBAction func handleSegmentControlAction(sender: AnyObject) {
    }
    
    @IBAction func handleSwitchAction(sender: AnyObject) {
       
        segmentControl.enabled = !segmentControl.enabled
    }
}
