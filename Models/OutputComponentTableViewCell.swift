//
//  OutputElementTableViewCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 12/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class OutputComponentTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var lblSliderValue: UILabel!
    
    @IBOutlet weak var viewSliderView: UISlider!
    
    @IBOutlet weak var lblBiBoxPort: UILabel!
    @IBOutlet weak var switchActivateControl: UISwitch!
    @IBOutlet weak var lblOutputComponent: UILabel!
   
    override func awakeFromNib()
    {
        super.awakeFromNib()
        viewSliderView.maximumValue = 65535
        viewSliderView.minimumValue = 0
        lblSliderValue.text = String(viewSliderView.value)
    }
    
    @IBAction func handleSwitchState(sender: UISwitch)
    {
        
        self.lblSliderValue.text = "0"
        self.viewSliderView.setValue(0, animated: true)
        
        viewSliderView.enabled = !viewSliderView.enabled
        lblBiBoxPort.enabled = !lblBiBoxPort.enabled
        lblSliderValue.enabled = !lblSliderValue.enabled
//        if !sender.on
//        {
//            viewSliderView.enabled = false
//           // lblOutputComponent.enabled = false
//            lblBiBoxPort.enabled = false
//            lblSliderValue.enabled = false
//        }
//        else
//        {
//            viewSliderView.enabled = true
//            //lblOutputComponent.enabled = true
//            lblBiBoxPort.enabled = true
//            lblSliderValue.enabled = true
//        }
        
//        viewSliderView.enabled =  Bool(switchActivateControl.state.rawValue)
//        // lblOutputComponent.enabled = false
//        lblBiBoxPort.enabled = Bool(switchActivateControl.state)
//        lblSliderValue.enabled = Bool(switchActivateControl.state.rawValue)
    }
    
    @IBAction func handleSliderState(sender: UISlider)
    {
        lblSliderValue.text = String(Int(sender.value))
    }
}
