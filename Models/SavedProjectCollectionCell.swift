//
//  SavedProjectCollectionCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 21/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class SavedProjectCollectionCell: UICollectionViewCell
{
    
    
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var viewInfo: UIView!
    
    @IBOutlet weak var lblProjName: UILabel!
    
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
}
