//
//  PickerTableViewCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 10/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class PickerTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    var arr: NSMutableArray = []
    override func awakeFromNib() {
        super.awakeFromNib()
        pickerView.delegate = self
        pickerView.dataSource = self
        self.setArrayValues()
    }
    
    func setArrayValues()
    {
        for i in 0...255
            
        {
            arr.addObject(i)
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func refreshValue()
    {
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return arr.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(arr[row])
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: "\(arr.objectAtIndex(row))", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        return attributedString
    }
}
