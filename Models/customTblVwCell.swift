//
//  customTblVwCell.swift
//  Bisoft1
//
//  Created by Bhagat Solanki on 13/10/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit

class customTblVwCell: UITableViewCell
{
    
    @IBOutlet var imgView: UIImageView!

    @IBOutlet weak var backView: UIView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
    }
    
}
