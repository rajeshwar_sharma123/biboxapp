//
//  IFTableViewCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 18/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class IFTableViewCell: UITableViewCell, DropDownDelegate
{
    @IBOutlet weak var btnComponents: UIButton!
    var delegate: InBetweenDelegate!
    
  //  @IBOutlet weak var tableView: UITableView!
   // @IBOutlet weak var viewShowHide: UIView!
   // var delegate: DropDownDelegate!
    var dropDown: DropDownView!
    
    @IBOutlet weak var btnCondition: UIButton!
    var btnActive = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
          let screenSize: CGRect = UIScreen.mainScreen().bounds
        dropDown = NSBundle.mainBundle().loadNibNamed("DropDownView", owner: self, options: nil)[0] as! DropDownView
        dropDown.delegate = self
        dropDown.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
    }
    
    
    @IBAction func btnComponentsAction(sender: UIButton)
    {
               btnActive = 1
        checkForDropDownHeight()
        dropDown.tableViewLeadingConstraint.constant = btnComponents.frame.origin.x
        dropDown.tableViewWidthConstraint.constant = btnComponents.frame.width
        
        layoutIfNeeded()
        
        dropDown.id = 1
        dropDown.tableView.reloadData()
        self.window?.addSubview(dropDown)
    }
    
    
    @IBAction func btnConditionAction(sender: UIButton)
    {
       
        btnActive = 2
        checkForDropDownHeight()
         dropDown.tableViewLeadingConstraint.constant = btnCondition.superview!.frame.origin.x + btnCondition.frame.origin.x
          dropDown.tableViewWidthConstraint.constant = btnCondition.frame.width
        layoutIfNeeded()
        
        dropDown.id = 2
        dropDown.tableView.reloadData()
        self.window?.addSubview(dropDown)

    }
    
    func updateValueOfButton(value: String)
    {
        
        if btnActive == 1
        {
            btnComponents.setTitle(value, forState: UIControlState.Normal)
            
        }
        else if btnActive == 2
        {
             btnCondition.setTitle(value, forState: UIControlState.Normal)
            if value == "IN BETWEEN"
            {
                delegate.manageExtraCell(true)
            }
            else
            {
                delegate.manageExtraCell(false)
            }
        }
        
    }
    
    func checkForDropDownHeight()
    {
       //  dropDown.count = 10
        let totalSizeOfcells: CGFloat = 10
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
       
        if 30 * totalSizeOfcells >= screenSize.height - 150
        {
            dropDown.tableViewHeightConstraint.constant = screenSize.height - 150
        }
        else
        {
            dropDown.tableViewHeightConstraint.constant = 30 * totalSizeOfcells
        }
        
        dropDown.tableView.center = CGPointMake(screenSize.width/2, screenSize.height/2)
       
        layoutIfNeeded()
      //  print(dropDown.tableView.frame)

        
    }
}
