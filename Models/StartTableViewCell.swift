//
//  StartTableViewCell.swift
//  BiSoft
//
//  Created by Devesh Rawat on 19/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//



import UIKit

class StartTableViewCell: UITableViewCell
{
    var delegate: SetFlagsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBOutlet weak var switchBicounter1: UISwitch!
    
    @IBOutlet weak var switchBicounter2: UISwitch!
    @IBOutlet weak var switchBicounter3: UISwitch!
    @IBOutlet weak var switchBiFlag1: UISwitch!
    @IBOutlet weak var switchBiFlag2: UISwitch!
    @IBOutlet weak var switchBiFlag3: UISwitch!
    @IBOutlet weak var switchBiData1: UISwitch!
    @IBOutlet weak var switchBiData2: UISwitch!
    @IBOutlet weak var switchBiData3: UISwitch!
    @IBOutlet weak var switchBluetooth: UISwitch!
    
    
    @IBAction func handleSwitchState(sender: UISwitch) {
        
        tag = sender.tag + 30
        
        if sender.on
        {
            delegate.arrFlags.append(tag)
        }
        else
        {
            delegate.arrFlags.removeAtIndex(delegate.arrFlags.indexOf(tag)!)
            
        }
        
    }
}