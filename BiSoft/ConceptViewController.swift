//
//  ConceptViewController.swift
//  Bisoft1
//
//  Created by Bhagat Solanki on 07/10/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit

var images: NSMutableArray = NSMutableArray()
var imagesName: NSMutableArray = NSMutableArray()
var scrollTblViewImages: NSMutableArray = NSMutableArray()
var scrollTblViewImagesName: NSMutableArray = NSMutableArray()
var screenShotImage: UIImage?

class ConceptViewController: UIViewController, iCarouselDataSource, iCarouselDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource
{
    var dataArray: NSMutableArray = NSMutableArray()
    var scrollTbleViewData: NSMutableArray = NSMutableArray()
    var popUpView:SaveXib!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var viewCaroseal: iCarousel!
    
    @IBOutlet weak var detailLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var imageTblView: UITableView!
    
    @IBOutlet weak var conceptUpperView: UIView!
    
    var selectedIndex:Int!
    var carouselIndexVal: Int!
    var imageView: UIImageView!
    var itemBackView: UIView!
    var arrMetadata: NSArray!
    var path: String!
    var isNewProject: Bool = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        viewCaroseal.type = iCarouselType.CoverFlow
        viewCaroseal.backgroundColor = UIColor.clearColor()
        imageTblView.separatorColor = UIColor.clearColor()
        imageTblView.backgroundColor = UIColor.grayColor()
        view.addSubview(imageTblView)
        images.removeAllObjects()
        imagesName.removeAllObjects()
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        carouselIndexVal = 0
        nameLbl.text = "Tern is Selected"
    }
    
    override func viewDidAppear(animated: Bool) {
        initGalleryItems()
        viewCaroseal.reloadData()
        screenShotMethod()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func screenShotMethod()
    {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        screenShotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(screenShotImage!, nil, nil, nil)
    }
    
    private func initGalleryItems()
    {
        let inputFile = NSBundle.mainBundle().pathForResource("Bibox_Metadata", ofType: "plist")
        
        let dictInputData = NSDictionary(contentsOfFile: inputFile!)
        
        arrMetadata = dictInputData?.objectForKey("Tern") as! NSArray
        print(appDelegate.currentProj)
        if(appDelegate.currentProj["Used_Components"] != nil){
            isNewProject = false
            let usedComponents = appDelegate.currentProj.objectForKey("Used_Components") as! NSMutableArray
            scrollTbleViewData = usedComponents.mutableCopy() as! NSMutableArray
            imageTblView.reloadData()
            for allComponent in arrMetadata{
                var elementDoesExist = false
                for selectedComponent in scrollTbleViewData{
                    if(allComponent.valueForKey("Name") as! String == selectedComponent.valueForKey("Name") as! String){
                        elementDoesExist = true
                    }
                }
                if(elementDoesExist == false)
                {
                    dataArray.addObject(allComponent)
                }
            }
        }else{
            isNewProject = true
            for item in arrMetadata
            {
                dataArray.addObject(item)
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: icarousel delegate methods
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return dataArray.count
    }
    
    //To find the current index of carousel
    func carouselCurrentItemIndexDidChange(carousel: iCarousel)
    {
        carouselIndexVal = carousel.currentItemIndex
        if (carouselIndexVal == -1)
        {
            carousel.currentItemIndex = 0
        }
        else
        {
            detailLbl.text = dataArray.objectAtIndex(carouselIndexVal).valueForKey("Description") as? String
            detailLbl.layer.borderColor = UIColor.blackColor().CGColor
            detailLbl.layer.cornerRadius = 8
            detailLbl.layer.borderWidth = 1
        }
        
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var itemView: UIImageView
        let label: UILabel
        //Label Code
        //let label = UILabel(frame: CGRect(x: 10, y: 85, width: 120, height: 21))
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            itemBackView = UIView(frame: CGRect(x:0, y:0, width:300, height:232))
            label = UILabel(frame: CGRect(x: 10, y: 200, width: 280, height: 21))
            label.textAlignment = .Center
            label.font = UIFont.boldSystemFontOfSize(16)
        }else{
            label = UILabel(frame: CGRect(x: 10, y: 85, width: 120, height: 21))
            label.textAlignment = .Center
            label.font = UIFont.boldSystemFontOfSize(10)
            itemBackView = UIView(frame: CGRect(x:0, y:0, width:150, height:112))
        }
        itemBackView.layer.cornerRadius = 8
        itemBackView.layer.borderWidth = 4
        itemBackView.layer.borderColor = UIColor.whiteColor().CGColor
        itemBackView.clipsToBounds = true
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            itemView = UIImageView(frame:CGRect(x:25, y:12, width:250, height:180))
        }else {
            itemView = UIImageView(frame:CGRect(x:25, y:8, width:100, height:80))
        }
        
        itemView.contentMode = .Center
        itemView.layer.cornerRadius = 8
        itemView.clipsToBounds = true
        itemView.backgroundColor = UIColor.whiteColor()
        itemView.contentMode = .ScaleAspectFit
        
        if (view == nil)
        {
            //itemView = UIImageView(frame:CGRect(x:0, y:0, width:150, height:112))
            let item : NSDictionary = dataArray.objectAtIndex(index) as! NSDictionary
            let typeOfImage = item.objectForKey("Type") as! String
            
            if(typeOfImage == "Input")
            {
                itemBackView.backgroundColor = UIColor(red: 244/255, green: 182/255, blue: 57/255, alpha: 1)
            }
            else
            {
                itemBackView.backgroundColor = UIColor(red: 0, green: 148/255, blue: 153/255, alpha: 1)
            }
            
            
            label.text = item.objectForKey("Name") as? String
            label.textAlignment = .Center
            
            itemView.image = UIImage(named: "\(item.valueForKey("ImageName")!)")
            itemView.userInteractionEnabled = true
            itemBackView.addSubview(label)
            itemBackView.addSubview(itemView)
            
        }
        else
        {
            let item2 : NSDictionary = dataArray.objectAtIndex(index) as! NSDictionary
            let typeOfImage = item2.objectForKey("Type") as! String
            
            if(typeOfImage == "Input")
            {
                itemBackView.backgroundColor = UIColor(red: 244/255, green: 182/255, blue: 57/255, alpha: 1)
            }
            else
            {
                itemBackView.backgroundColor = UIColor(red: 0, green: 148/255, blue: 153/255, alpha: 1)
            }
            for item in (view?.subviews)! {
                if(item.isKindOfClass(UILabel)){
                    (item as! UILabel).text = item2.objectForKey("Name") as? String
                    itemBackView.addSubview(item)
                }else if(item.isKindOfClass(UIImageView)){
                    (item as! UIImageView).contentMode = .Center
                    (item as! UIImageView).layer.cornerRadius = 8
                    (item as! UIImageView).layer.borderWidth = 2
                    (item as! UIImageView).layer.borderColor = UIColor.whiteColor().CGColor
                    (item as! UIImageView).clipsToBounds = true
                    (item as! UIImageView).userInteractionEnabled = true
                    (item as! UIImageView).image = UIImage(named: "\(item2.valueForKey("ImageName")!)")
                    itemBackView.addSubview(item)
                }
            }
        }
        
        
        //return itemView
        return itemBackView
        
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int)
    {
        //MARK: TO Remove and image from carousel and adding it in tableview
        scrollTbleViewData.addObject(dataArray.objectAtIndex(carouselIndexVal))
        dataArray.removeObjectAtIndex(carouselIndexVal)
        imageTblView.reloadData()
        carousel.reloadData()
        if(index == dataArray.count){
            carousel.scrollToItemAtIndex(index, animated: true)
        }
        //Reload the Label
        carouselCurrentItemIndexDidChange(carousel)
        
    }
    
    //MARK: TableView Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return scrollTbleViewData.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell:customTblVwCell = tableView.dequeueReusableCellWithIdentifier("tblViewCell") as! customTblVwCell
        cell.imgView.image = UIImage(named: "\(scrollTbleViewData.objectAtIndex(indexPath.row).valueForKey("ImageName")!)")
        cell.backView.backgroundColor = UIColor.whiteColor()
        var componentType = scrollTbleViewData.objectAtIndex(indexPath.row).valueForKey("Type")! as! String
        if(componentType == "Input"){
            cell.backView.layer.borderColor = UIColor(red: 244/255, green: 182/255, blue: 57/255, alpha: 1).CGColor
        }else{
            cell.backView.layer.borderColor = UIColor(red: 0, green: 148/255, blue: 153/255, alpha: 1).CGColor
        }
        cell.backView.layer.borderWidth = 3
        cell.backView.layer.cornerRadius = 9
        cell.backView.clipsToBounds = true
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            return 160
        }else{
            return 113
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            scrollTbleViewData.removeObjectAtIndex(indexPath.row)
            dataArray.removeAllObjects()
            
            dataArray = arrMetadata.mutableCopy() as! NSMutableArray
            
//            for i in 0..<(dataArray.count-scrollTbleViewData.count)
//            {
//                let item = arrMetadata.objectAtIndex(i)
//                for selectedItem in scrollTbleViewData
//                {
//                    if(item.valueForKey("Name") as! String == selectedItem.valueForKey("Name") as! String){
//                        dataArray.removeObjectAtIndex(i)
//                    }
//                }
//            }
            for i in 0..<(scrollTbleViewData.count){
                let selectedItem = scrollTbleViewData.objectAtIndex(i)
                for j in 0..<(dataArray.count){
                    let item = dataArray.objectAtIndex(j)
                    if(item.valueForKey("Name") as! String == selectedItem.valueForKey("Name") as! String){
                        dataArray.removeObjectAtIndex(j)
                        break
                    }
                }
            }
            viewCaroseal.reloadData()
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        else if editingStyle == .Insert
        {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    
    
    //Save Button Functionality
    @IBAction func saveButtonPressed(sender: AnyObject)
    {
        appDelegate.currentProj.setValue(scrollTbleViewData, forKey: "Used_Components")
        let saveview: SaveXib = NSBundle.mainBundle().loadNibNamed("SaveXib", owner: self, options: nil)[0] as! SaveXib
        saveview.controllerBase = self
        if(isNewProject){
            saveview.isNewProject = true
        }else{
            saveview.isNewProject = false
        }
        self.view.window?.addSubview(saveview)
        
    }
    @IBAction func handleBackButtonAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnLogicFlowAction(sender: AnyObject)
    {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LogicFlowViewController2") as?LogicFlowViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}



