//
//  Utils.swift
//  BiSoft
//
//  Created by Devesh Rawat on 18/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

protocol DropDownDelegate {
    
    func updateValueOfButton(value: String)
    
    
}

protocol InBetweenDelegate {
    
    func manageExtraCell(inBetweenPressed: Bool)
}

protocol SetFlagsDelegate {
    
    var arrFlags: [Int]{get set}
   // func setTheFlags(arr: Int)
}

protocol ShowHideSettigsViewDelegate
{
  func handleShowHideAction()
}