//
//  ExploreViewController.swift
//  Bisoft1
//
//  Created by Bhagat Solanki on 20/10/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController
{
      
    @IBOutlet weak var exploreImageVw: UIImageView!
    
    
    @IBOutlet weak var exploreScrollVw: UIScrollView!
    
    
    @IBOutlet weak var exploreScrollUprVw: UIView!
   
    @IBOutlet weak var exploreScrollUprVwHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var exploreScreenshotVw: UIImageView!
    
    @IBOutlet weak var projectNameLbl: UILabel!
    
    @IBOutlet weak var biboxTypeLbl: UILabel!
    
    @IBOutlet weak var componentUsedTxtVw: UITextView!
    
    @IBOutlet weak var componentUsedLbl: UILabel!
    
    @IBOutlet weak var btnUpload: UIButton!
    
    @IBOutlet weak var btnExplore: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        //Screenshot Image
        // exploreScreenshotVw.image =  screenShotImage
        
//        
//        if let path = NSBundle.mainBundle().pathForResource("uniqueProjectId ", ofType: "plist")
//        {
//            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject>
//            {
//                print(dict)
//            }
//        }
        
        btnUpload.layer.cornerRadius = 5
        btnExplore.layer.cornerRadius = 5
        
        let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let project =  appdelegate.currentProj
        
        projectNameLbl.text = project.objectForKey("Name_Of_Project") as? String
        componentUsedLbl.text = project.objectForKey("Description") as? String
        
        
    }
    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func exploreButtonTapped(sender: AnyObject)
    {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ConceptViewController") as?ConceptViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func uploadProgramButtonTapped(sender: AnyObject)
    {
//        projectNameLbl.text = name
//        componentUsedLbl.text = description1
//        for image in scrollTblViewImages
//        {
//            
//            var temp:String = ""
//            let name = image
//            if(scrollTblViewImages.indexOfObject(image) == 0){
//                temp = String(scrollTblViewImages.indexOfObject(image)) + ". " + (name as! String)
//                componentUsedTxtVw.text=temp
//                
//            }
//            else
//            {
//                
//                temp = String(scrollTblViewImages.indexOfObject(image)) + ". " + (name as! String)
//                componentUsedTxtVw.text = componentUsedTxtVw.text +  " , "  + temp
//            }
//        }

    }
    
    
    @IBAction func handleBackButton(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    

}
