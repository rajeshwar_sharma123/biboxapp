//
//  ProjectViewController.swift
//  BiSoft
//
//  Created by Devesh Rawat on 21/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit


class ProjectsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate
{
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var viewShowHide: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnStart: UIButton!
    var arrKeys = []
    
    var arrMenu = ["EDIT", "DELETE"]
    var width = Int()
    
    var height = Int()
    
    var plistData = NSMutableDictionary()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        arr = [1,2,3,4,5,6,7,8]
        
        tableView.registerNib(UINib(nibName: "DropDownTableViewCell", bundle: nil) ,forCellReuseIdentifier: "DropDownTableViewCell")
        viewShowHide.hidden = true
        
       
        let containerView:UIView = UIView(frame:self.tableView.frame)
        containerView.backgroundColor = UIColor.clearColor()
        containerView.layer.shadowColor = UIColor.darkGrayColor().CGColor
        containerView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 5
        
        self.viewShowHide.addSubview(containerView)
        containerView.addSubview(tableView)
        
        let longPGR = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        longPGR.minimumPressDuration = 0.5
        longPGR.delegate = self
        longPGR.delaysTouchesBegan = true
        collectionView.addGestureRecognizer(longPGR)
      
    }
   
    override func viewWillAppear(animated: Bool) {
        
       loadDataFromDocuments()
        
    }
    
    func loadDataFromDocuments()
    {
        let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appdelegate.currentProj.removeAllObjects()
        
        let fileManager = NSFileManager.defaultManager()
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let path = documentDirectory.stringByAppendingString("/ProjectList.plist")
        
        
        
        if(fileManager.fileExistsAtPath(path))
        {
            plistData = NSDictionary(contentsOfFile: path)?.mutableCopy() as! NSMutableDictionary
            arrKeys = plistData.allKeys
        }
        else
        {
            arrKeys = []
            
        }
        print(plistData)
        
    }
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        print(collectionView.frame.width)
        print(collectionView.frame.height)
        
        width = Int((collectionView.frame.width - 8 ) / 5)
        height = Int(CGFloat(width) * 1.5)
        print(width)
        print(height)
        
        collectionView.reloadData()
    }
    
    func roundTheCorners(cell: SavedProjectCollectionCell)
    {
        
       let path = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.TopLeft, .TopRight], cornerRadii: CGSize(width: 20, height:  20))
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.CGPath
        cell.layer.mask = maskLayer
        
        btnStart.layer.cornerRadius = 5
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return arrKeys.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: SavedProjectCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("CELL", forIndexPath: indexPath) as! SavedProjectCollectionCell
        cell.lblProjName.text = plistData.valueForKey("\(arrKeys.objectAtIndex(indexPath.row))")!["Name_Of_Project"] as? String
         cell.lblDescription.text = plistData.valueForKey("\(arrKeys.objectAtIndex(indexPath.row))")!["Description"] as? String
        cell.lblType.text = "ULTRA"
        roundTheCorners(cell)
        return cell
        
    }
    
    
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
     
       return CGSizeMake(CGFloat(width), CGFloat(height))
      
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appdelegate.currentProj = plistData.objectForKey("\(arrKeys.objectAtIndex(indexPath.row))") as! NSMutableDictionary
        
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc : ExploreViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ExploreViewController") as! ExploreViewController
        self.navigationController?.pushViewController(vc, animated: true)

        
    }

    @IBAction func btnStartAction(sender: UIButton) {
        
      let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        
//        appdelegate.currentProj = [
//                                   "ID": "",
//                                   "Name_Of_Project": "",
//                                   "Description": "",
//                                   "Image": "",
//                                   "Bibox_Type": "",
//                                  ]
         appdelegate.currentProj.removeAllObjects()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ConceptViewController") as?ConceptViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func btnBackAction(sender: UIButton) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // handling of edit menu for each saved project
    
    
    @IBAction func handleShowHideButtonAction(sender: AnyObject)
    {
        
        viewShowHide.hidden = true
    }
    
    
    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer)
    {
//        if gestureReconizer.state != UIGestureRecognizerState.Ended {
//            return
//        }
        
        let p = gestureReconizer.locationInView(self.collectionView)
        let indexPath = self.collectionView.indexPathForItemAtPoint(p)
        
        if let index = indexPath {
          //  var cell = self.collectionView.cellForItemAtIndexPath(index)
            // do stuff with your cell, for example print the indexPath
            viewShowHide.hidden = false
             let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appdelegate.currentProj = self.plistData.objectForKey("\(self.arrKeys.objectAtIndex(index.row))") as! NSMutableDictionary
            print(index.row)
        }
        else
        {
            print("Could not find index path")
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    
     func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     
        let title = "Select Option"
        
        let longestWordRange = (title as NSString).rangeOfString(title)

        let attributedString = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(20)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFontOfSize(20), NSForegroundColorAttributeName : UIColor.blueColor()], range: longestWordRange)
        
        return title
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DropDownTableViewCell") as! DropDownTableViewCell
       
        cell.lblName.text = arrMenu[indexPath.row]
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        viewShowHide.hidden = true
//        let cell = tableView.cellForRowAtIndexPath(indexPath)
//        cell?.selected = false
        let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
      //  let fileManager = NSFileManager.defaultManager()
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let path = documentDirectory.stringByAppendingString("/ProjectList.plist")

        switch indexPath.row {
        case 0:
        
            viewShowHide.hidden = true
            
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc : SaveProjectViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SaveProjectViewController") as! SaveProjectViewController
            vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)

        case 1:
            
            let alert = UIAlertController(title: "Delete File", message: "Are you sure! you want to delete this file.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Keep It", style: UIAlertActionStyle.Default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Delete",
                style: UIAlertActionStyle.Default,
                handler: {(alert: UIAlertAction!) in
                    
                    
                 self.plistData.removeObjectForKey(appdelegate.currentProj.valueForKey("ID") as! String)
                    
                    if self.plistData.count == 0
                    {
                        do {
                            try NSFileManager.defaultManager().removeItemAtPath(path)
                            print("old image has been removed")
                        } catch {
                            print("an error during a removing")
                        }
                    }
                    else
                    {
                       self.plistData.writeToFile(path, atomically: true)
                       
                    }
                    self.loadDataFromDocuments()
                    self.collectionView.reloadData()
            }))
            
            self.presentViewController(alert, animated: true, completion:nil)
            
          
            
            
        default:
            print("Show error messages")
            break
        }
         tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    }

