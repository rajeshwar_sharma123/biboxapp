//
//  SaveProjectViewController.swift
//  Bisoft1
//
//  Created by Bhagat Solanki on 19/10/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

import UIKit



class SaveProjectViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate
{

    var path: String!
    var uniqueProjectId: String = ""
    var name:String!
    var description1: String!
    
    @IBOutlet weak var projectDetailView: UIView!
    
    @IBOutlet weak var projectImageUnderView: UIView!
    
   
    @IBOutlet weak var projectImageView: UIImageView!
    
    @IBOutlet weak var projectNameTxtField: UITextField!
    
    @IBOutlet weak var projectDescriptionTxtField: UITextField!
    
    @IBOutlet weak var componentDetailTxtField: UITextView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var urlTxtField: UITextField!
    
    
    @IBOutlet weak var browseVideoBtn: UIButton!
    var Usedcomponents = NSMutableArray()
    
    @IBOutlet weak var btnSaveProject: UIButton!
    var projectName: String?
    var projectDescription: String?
    var isEdit = false
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        projectNameTxtField.delegate = self
        projectDescriptionTxtField.delegate = self
        urlTxtField.delegate = self
        
//        for image in scrollTblViewImages
//        {
//            
//            var temp:String = ""
//            let name = image
//            if(scrollTblViewImages.indexOfObject(image) == 0){
//                temp = String(scrollTblViewImages.indexOfObject(image)) + ". " + (name as! String)
//                componentDetailTxtField.text=temp
//
//            }
//            else
//            {
//            
//            temp = String(scrollTblViewImages.indexOfObject(image)) + ". " + (name as! String)
//            componentDetailTxtField.text = componentDetailTxtField.text +  " , "  + temp
//            }
//        }
        

    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        let paddingForFirst = UIView(frame: CGRectMake(0, 0, 15, self.projectNameTxtField.frame.size.height))
        projectNameTxtField.leftView = paddingForFirst
        projectNameTxtField.leftViewMode = UITextFieldViewMode .Always

        let paddingForSecond = UIView(frame: CGRectMake(0, 0, 15, self.projectNameTxtField.frame.size.height))
        projectDescriptionTxtField.leftView = paddingForSecond
        projectDescriptionTxtField.leftViewMode = UITextFieldViewMode .Always
        
        let paddingForThird = UIView(frame: CGRectMake(0, 0, 15, self.urlTxtField.frame.size.height))
        urlTxtField.leftView = paddingForThird
        urlTxtField.leftViewMode = UITextFieldViewMode .Always
        
        projectName = projectNameTxtField.text
        projectDescription = projectDescriptionTxtField.text
        
        
        projectNameTxtField.layer.borderWidth = 1
        projectNameTxtField.layer.borderColor = UIColor(red: 22/255.0, green: 128/255.0, blue: 226/255.0, alpha: 1.0).CGColor
        projectNameTxtField.layer.cornerRadius = 5
        
        projectDescriptionTxtField.layer.borderWidth = 1
        projectDescriptionTxtField.layer.borderColor = UIColor(red: 22/255.0, green: 172/255.0, blue: 226/255.0, alpha: 1.0).CGColor
        projectDescriptionTxtField.layer.cornerRadius = 5
        //Project ImageView Screenshot
      
        
        //projectImageView.image =  screenShotImage
        
        componentDetailTxtField.layer.borderWidth = 1
        componentDetailTxtField.layer.cornerRadius = 5
        componentDetailTxtField.layer.borderColor = UIColor(red: 22/255.0, green: 172/255.0, blue: 226/255.0, alpha: 1.0).CGColor
        
        urlTxtField.layer.cornerRadius = 5
        componentDetailTxtField.layer.cornerRadius = 5
        btnSaveProject.layer.cornerRadius = 5
        browseVideoBtn.layer.cornerRadius = 5
        
        urlTxtField.layer.borderColor = UIColor(red: 22/255.0, green: 172/255.0, blue: 226/255.0, alpha: 1.0).CGColor
        urlTxtField.layer.borderWidth = 1
        urlTxtField.layer.cornerRadius = 5
        browseVideoBtn.hidden = false
        urlTxtField.hidden = true
        
        
        let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        print(appdelegate.currentProj)
        
        
        if appdelegate.currentProj["Used_Components"] != nil
        {
            Usedcomponents = appdelegate.currentProj.valueForKey("Used_Components") as! NSMutableArray
            print(Usedcomponents)
            for item in appdelegate.currentProj["Used_Components"] as! NSMutableArray
            {
                componentDetailTxtField.text.appendContentsOf(item["Name"] as! String)
            }
        }

        if isEdit
        {
            projectNameTxtField.text = appdelegate.currentProj.valueForKey("Name_Of_Project") as? String
            projectDescriptionTxtField.text = appdelegate.currentProj.valueForKey("Description") as? String
        }
        

    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.projectNameTxtField
        {
            projectDescriptionTxtField.becomeFirstResponder()
            return true
        }
        
//        if textField == self.projectDescriptionTxtField
//        {
            self.view.endEditing(true)
           return false
     //   }
        
    }
    

    
    @IBAction func browseVideoButtonTapped(sender: AnyObject)
    {
//        let ImagePicker = UIImagePickerController()
//        ImagePicker.delegate = self
//        ImagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//        
//        self.presentViewController(ImagePicker, animated: true, completion: nil)
//        
    }
    @IBAction func saveButtonTapped(sender: AnyObject)
    {
        //Unique ID For Project
        let date = NSDate()
        let timestamp = Int64(date.timeIntervalSince1970 * 1000.0)
        uniqueProjectId = String(timestamp)
        print(uniqueProjectId)
        
        let fileManager = NSFileManager.defaultManager()
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        projectName = projectName! + ".plist"
        //path = documentDirectory.stringByAppendingString("/profile.plist")
        path = documentDirectory.stringByAppendingString("/ProjectList.plist")
        
        
        var plistData = NSMutableDictionary()
        
        if(fileManager.fileExistsAtPath(path))
        {
            plistData = NSDictionary(contentsOfFile: path)?.mutableCopy() as! NSMutableDictionary
        }
        
        print(path)
        
        if !isEdit
        {
        
        
        let data = [
                    "ID": "\(uniqueProjectId)",
                    "Name_Of_Project": "\(projectNameTxtField.text!)",
                    "Description": "\(projectDescriptionTxtField.text!)",
                    "Image": "\(screenShotImage)",
                    "Bibox_Type": "",
                    "Used_Components":  Usedcomponents
                        
                    ]
        
//        let data3 = ["\(uniqueProjectId)":data]
        
        print(data["Used_Components"])
        plistData.setObject(data, forKey: "\(uniqueProjectId)")
            
//        let someData = NSDictionary(dictionary: data3)
        let isWritten = plistData.writeToFile(path, atomically: true)
        print("is the file created: \(isWritten)")
//        let dict1 = NSDictionary(contentsOfFile: path as String)
//        print("dict",dict1)
//        print(dict1?.objectForKey(uniqueProjectId)?.valueForKey("Name_Of_Project"))
//        name = dict1?.objectForKey(uniqueProjectId)?.valueForKey("Name_Of_Project") as? String
//        description1 = dict1?.objectForKey(uniqueProjectId)?.valueForKey("Description") as? String
     //   print(dict1?.objectForKey(uniqueProjectId)?.valueForKey())
        }
        
        else
        {
        
            let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appdelegate.currentProj.setValue(projectNameTxtField.text, forKey: "Name_Of_Project")
            appdelegate.currentProj.setValue(projectDescriptionTxtField.text, forKey: "Description")
            
            plistData.setObject(appdelegate.currentProj, forKey: appdelegate.currentProj.valueForKey("ID") as! String)
            plistData.writeToFile(path, atomically: true)
            isEdit = false
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func handleSegmentControlAction(sender: UISegmentedControl)
    {
        if sender.selectedSegmentIndex == 0
        {
            urlTxtField.hidden = true
            browseVideoBtn.hidden = false
        }
        else{
            
            urlTxtField.hidden = false
            browseVideoBtn.hidden = true
        }
    }

    @IBAction func handleBackButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
