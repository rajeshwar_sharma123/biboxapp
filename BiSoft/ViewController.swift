//
//  ViewController.swift
//  BiSoft
//
//  Created by Devesh Rawat on 05/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    @IBAction func btnEnterAction(sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ProjectsViewController") as?ProjectsViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
   
  }

