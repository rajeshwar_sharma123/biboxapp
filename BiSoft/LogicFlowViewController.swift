//
//  LogicFlowViewController.swift
//  BiSoft
//
//  Created by Devesh Rawat on 05/10/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

class LogicFlowViewController: UIViewController,  UIScrollViewDelegate, ShowHideSettigsViewDelegate
{
    
    
    @IBOutlet weak var viewOptionsView: UIView!
    @IBOutlet weak var viewOptionInsertDelete: UIView!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    //    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var viewScrollViewBG: UIView!
    
    var rowCount = 30
    
    var colCount = 30
    
    //    var activeButtonStack: NSMutableArray = []
    
    var hexButtonArray: NSMutableArray = []
    
    var arrArrowStack: NSMutableArray = []
    
    var settingView : SettingsView!
    
    var activeButton: UIButton!
    var activeDeleteButton: UIButton!
    var activeInsertButton: UIButton!
    var activeArrow: UIImageView!
    
    var tagLastButton: Int = 0
    
    var tagLastArrow = 1
    
    var maxNodeIndex = 0
    
    var activeNodeIndex = 0
    
    var shouldLeaveExtraSpace = 0
    
    var hasLeftArrow = false
    
    @IBOutlet weak var btn1OptonsView: UIButton!
    @IBOutlet weak var btn2OptionsView: UIButton!
    @IBOutlet weak var btn3OptionsView: UIButton!
    @IBOutlet weak var btn4OptionsView: UIButton!
    @IBOutlet weak var btn5OptionsView: UIButton!
    @IBOutlet weak var btnMiddleOptionsView: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var optionViewXconstraint: NSLayoutConstraint!
    @IBOutlet weak var optionsViewYconstraint: NSLayoutConstraint!
    @IBOutlet weak var viewScrollViewBGHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewScrollViewBGWidthConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let screenSize = UIScreen.mainScreen().bounds
        
        scrollview.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        scrollview.delegate = self
        scrollview.decelerationRate = UIScrollViewDecelerationRateFast
        
        btn1OptonsView.imageView?.contentMode = UIViewContentMode.Center
        btn2OptionsView.imageView?.contentMode = UIViewContentMode.Center
        btn3OptionsView.imageView?.contentMode = UIViewContentMode.Center
        btn4OptionsView.imageView?.contentMode = UIViewContentMode.Center
        btn5OptionsView.imageView?.contentMode = UIViewContentMode.Center
        btnMiddleOptionsView.imageView?.contentMode = UIViewContentMode.Center
        
        //        optionsViewYconstraint.active = false
        //        optionsViewYconstraint.active = false
        viewOptionsView.removeFromSuperview()
        viewScrollViewBG.addSubview(viewOptionsView)
        viewOptionsView.hidden = true
        viewOptionsView.layer.zPosition = 1
        
        viewOptionInsertDelete.removeFromSuperview()
        viewScrollViewBG.addSubview(viewOptionInsertDelete)
        viewOptionInsertDelete.hidden = true
        viewOptionInsertDelete.layer.zPosition = 1
        
        
        settingView = NSBundle.mainBundle().loadNibNamed("SettingsView", owner: self, options: nil)[0] as! SettingsView
      //  settingView.arrComponents = [17,0,6,16,1,2,3,4]
        let panGR = UIPanGestureRecognizer(target: self, action: #selector(self.showHideSettingsView))
        settingView.viewImageViewBG.addGestureRecognizer(panGR)
        settingView.delegate = self
        settingView.frame = CGRectMake(0,screenSize.height - 40 , screenSize.width, screenSize.height)
        self.view.addSubview(settingView)
      //  settingView.hidden = true
        
    }
    
    func handleShowHideAction()
    {
        
        let screenSize = UIScreen.mainScreen().bounds
        if settingView.isVisible
        {
            
            UIView.animateWithDuration(1, animations: {
                self.settingView.center = CGPointMake(self.view.center.x, self.view.center.y + screenSize.height - 40)
                
                }, completion: { (true) in
                    self.settingView.isVisible = false
            })
        }
        else
        {
            
            UIView.animateWithDuration(1, animations: {
                self.settingView.center = CGPointMake(self.view.center.x, self.view.center.y + 40)
                }, completion: { (true) in
                    self.settingView.isVisible = true
            })
            
        }
        
    }
    
override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.InitialiseNodes()
        
        viewScrollViewBG.backgroundColor = UIColor.init(patternImage: UIImage(named: "bg4.png")!)
    }
    
    func InitialiseNodes()
    {
        //        let arrFlow = [1,2,2,[5,[1,2,[5,[1,2,2]]]],1,[5,[1,2,[5,[1,2,2]]]]]
        
        tagLastButton = 0
        
        maxNodeIndex = 0
        
        activeNodeIndex = 0
        
        shouldLeaveExtraSpace = 0
        
        hexButtonArray.removeAllObjects()
        
        for item in viewScrollViewBG.subviews
        {
            if(item.isKindOfClass(UIButton) || item.isKindOfClass(UIImageView))
            {
                item.removeFromSuperview()
            }
        }
        
        var button = self.CreateNewButton(CGPointMake(72 * 2.5, 124 * 1.5))
        button.setImage(UIImage(named: "0.png"), forState: UIControlState.Normal)
        button.tag = tagLastButton
        viewScrollViewBG.addSubview(button)
        
        var dict = ["\(tagLastButton)":
            ["Value":"0",
                "address":button]]
        
        hexButtonArray.addObject(dict)
        
        tagLastButton += 1
        
        //---------------------------------------------------------------------
        
        button = self.CreateNewButton(CGPointMake(72 * 3.5, 124 * 1.5))
        
        button.tag = tagLastButton
        viewScrollViewBG.addSubview(button)
        
        let arrow = self.CreateArrowAtPoint(CGPointMake(72 * 3.5 - 30, 124 * 1.5))
        arrow.image = UIImage(named: "3_R.png")
        arrow.tag = tagLastArrow
        viewScrollViewBG.addSubview(arrow)
        
        arrArrowStack.addObject(arrow)
        hasLeftArrow = true
        
        dict = ["\(tagLastButton)":
            ["Value":"-1",
                "address":button]]
        
        hexButtonArray.addObject(dict)
        
        tagLastButton += 1
        activeNodeIndex = 1
        maxNodeIndex = 3
        
        //        self.RefreshFlowUI(activeButtonStack, point: CGPointMake(135, 139))
    }
    
    func showHideSettingsView(panGR: UIPanGestureRecognizer)
    {
        
        let screenSize = UIScreen.mainScreen().bounds
        //        let translation = panGR.translationInView(self.view)
        let velocity = panGR.velocityInView(self.view)
        
        switch panGR.state {
            
        case .Began:
            break
            
        case .Changed:
            
            if settingView.isVisible && velocity.y > 0
            {
                //                    settingView.center = CGPointMake((settingView.center.x) , (settingView.center.y) + translation.y)
                //                    panGR.setTranslation(CGPointMake(0, 0), inView: self.view)
                
                UIView.animateWithDuration(1, animations: {
                    self.settingView.center = CGPointMake(self.view.center.x, self.view.center.y + screenSize.height - 40)
                    
                    }, completion: { (true) in
                        self.settingView.isVisible = false
                })
                
                
            }
                
            else if !settingView.isVisible && velocity.y < 0
            {
                //                    settingView.center = CGPointMake((settingView.center.x) , (settingView.center.y) + translation.y)
                //                    panGR.setTranslation(CGPointMake(0, 0), inView: self.view)
                
                UIView.animateWithDuration(1, animations: {
                    self.settingView.center = CGPointMake(self.view.center.x, self.view.center.y + 40)
                    }, completion: { (true) in
                        self.settingView.isVisible = true
                })
                
            }
            
            break
            
        case .Ended:
            
            break
            
        default: break
            
        }
        
    }
    
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        updateMinZoomScaleForSize(view.bounds.size)
        
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView)
    {
        
        if scrollView.zoomScale < 1.0
        {
            viewScrollViewBG.backgroundColor = UIColor.clearColor()
        }
        else
        {
            viewScrollViewBG.backgroundColor = UIColor.init(patternImage: UIImage(named: "bg4.png")!)
        }
    }
    
    private func updateMinZoomScaleForSize(size: CGSize)
    {
        let widthScale = size.width / (viewScrollViewBG.bounds.width * 2)
        let heightScale = size.height / (viewScrollViewBG.bounds.height * 2)
        let minScale = min(widthScale, heightScale)
        
        scrollview.minimumZoomScale = minScale
        scrollview.maximumZoomScale = 1.0
        
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
        return viewScrollViewBG
    }
    
    
    func CreateNewButton(center: CGPoint) -> UIButton
    {
        let btnOption = UIButton(type: UIButtonType.Custom)
        btnOption.frame = CGRectMake(0, 0, 70, 78)
        
        btnOption.center = center
        
        btnOption.addTarget(self, action: #selector(LogicFlowViewController.HandleLogicButtonClicked), forControlEvents: UIControlEvents.TouchUpInside)
        
        btnOption.setImage(UIImage(named: "ActiveNode.png"), forState: UIControlState.Normal)
        btnOption.titleLabel?.text = "option"
        btnOption.imageView!.contentMode = UIViewContentMode.Center
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(LogicFlowViewController.longPressed(_:)))
        
        btnOption.addGestureRecognizer(longPressGesture)
        
        return btnOption
    }
    
    func CreateArrowAtPoint(center: CGPoint) -> UIImageView
    {
        let rightArrow = UIImageView(image: UIImage(named: "1_R.png"))
        rightArrow.layer.zPosition = 2
        //        var arrowCenter = center
        //        arrowCenter.x -= 30
        rightArrow.center = center
        
        activeArrow = rightArrow
        
        return rightArrow
    }
    
    func CreateDownArrowAtPoint(center: CGPoint) -> UIImageView
    {
        let downArrow = UIImageView(image: UIImage(named: "1_D.png"))
        downArrow.layer.zPosition = 2
        downArrow.center = center
        
        return downArrow
    }
    
    func longPressed(gesture: UIGestureRecognizer)
    {
        if (gesture.state != UIGestureRecognizerState.Began)
        {
            return
        }
        
        let sender: UIButton = gesture.view as! UIButton
        
        print(sender.tag)
        
        let centerFrame = sender.center
        //        centerFrame.x += 15
        viewOptionInsertDelete.center = centerFrame
        //        viewOptionsView.hidden = false
        viewOptionInsertDelete.hidden = false
        
        activeDeleteButton = sender
        activeInsertButton = sender
        
        for i in 1 ..< hexButtonArray.count
        {
            let dict = hexButtonArray.objectAtIndex(i) as! NSDictionary
            
            let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
            
            if(sender.tag == Int(key))
            {
                let value = Int(dict.objectForKey(key)!.objectForKey("Value") as! String)
                
                if(value == 3)
                {
                    btnDelete.hidden = true
                }
                else
                {
                    btnDelete.hidden = false
                }
                break
            }
        }
    }
    
    func HandleLogicButtonClicked(sender: UIButton)
    {
        if(activeButton != nil)
        {
            activeButton.setBackgroundImage(nil, forState: UIControlState.Normal)
        }
        
        if(!viewOptionInsertDelete.hidden || !viewOptionsView.hidden)
        {
            self.btnMiddleOptionViewAction(sender)
            return
        }
        
        activeButton = sender
        activeInsertButton = nil
        activeDeleteButton = nil
        
        var dict : NSDictionary = [:]
        var key :String = ""
        
        if(activeNodeIndex != hexButtonArray.count)
        {
            dict = hexButtonArray.objectAtIndex(activeNodeIndex) as! NSDictionary
            print(dict)
            key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
        }
        
        //        let buttonValue = Int(dict.objectForKey(key)!.objectForKey("Value") as! String)
        
        //        let btnActive = dict.objectForKey(key)!.objectForKey("address")
        
        //        print(sender)
        //        print(btnActive)
        
        if(sender.tag == Int(key) && activeNodeIndex < hexButtonArray.count)
        {
            let centerFrame = sender.center
            //            centerFrame.x += 25
            viewOptionsView.center = centerFrame
            viewOptionsView.hidden = false
            
            
            var i = activeNodeIndex
            
            var item = hexButtonArray.objectAtIndex(i) as! NSDictionary
            
            var counter = 0
            
            while(i > 0)
            {
                let key = (item.allKeys as NSArray).objectAtIndex(0) as! String
                let buttonValue = Int(item.objectForKey(key)!.objectForKey("Value") as! String)
                
                if(buttonValue == 3)
                {
                    counter += 1
                }
                if(buttonValue > 3 || i == 1)
                {
                    counter -= 1
                }
                
                if(counter < 0)
                {
                    if(buttonValue == 4)
                    {
                        btn3OptionsView.setImage(UIImage(named: "EndIf.png"), forState: UIControlState.Normal)
                    }
                    else if(buttonValue == 5)
                    {
                        btn3OptionsView.setImage(UIImage(named: "EndLoop.png"), forState: UIControlState.Normal)
                    }
                    else
                    {
                        btn3OptionsView.setImage(UIImage(named: "Repeat.png"), forState: UIControlState.Normal)
                    }
                    break
                }
                
                i -= 1
                item = hexButtonArray.objectAtIndex(i) as! NSDictionary
            }
            
            
            
            
            btn3OptionsView.hidden = false
        }
        else
        {
            for item in hexButtonArray
            {
                let key = (item.allKeys as NSArray).objectAtIndex(0) as! String
                
                if(Int(key) == sender.tag)
                {
                    let buttonValue = Int(item.objectForKey(key)!.objectForKey("Value") as! String)
                    createComponentsArray(buttonValue!)
                    settingView.hidden = false
                    
                    if(buttonValue >= 0)
                    {
                        sender.setBackgroundImage(UIImage(named: "SelectedNodeBG.png"), forState: UIControlState.Normal)
                    }
                    else
                    {
                        
                    }
                    break
                }
            }
        }
    }
    
    func SelectActiveButtonAsForward(type: Int, forButton sender: UIButton)
    {
        activeButton.setImage(UIImage(named: "\(type).png"), forState: UIControlState.Normal)
        
        var dict = ["\(activeButton.tag)":
            ["Value":"\(type)",
                "address":activeButton]]
        
        hexButtonArray.insertObject(dict, atIndex: activeNodeIndex)
        activeNodeIndex += 1
        
        //------------------Second Button---------------------------------
        let arrow = self.CreateArrowAtPoint(CGPointMake(activeButton.center.x + 72 - 30, activeButton.center.y))
        arrow.tag = tagLastArrow
        activeArrow.image = UIImage(named: "\(type)_R.png")
        viewScrollViewBG.addSubview(arrow)
        
        if(hasLeftArrow == true)
        {
            arrArrowStack.replaceObjectAtIndex(arrArrowStack.count - 1, withObject: arrow)
        }
        else
        {
            arrArrowStack.addObject(arrow)
            hasLeftArrow = true
        }
        
        let button: UIButton = self.CreateNewButton(CGPointMake(activeButton.center.x + 72, activeButton.center.y))
        button.tag = tagLastButton
        viewScrollViewBG.addSubview(button)
        
        dict = ["\(tagLastButton)":
            ["Value":"-1",
                "address":button]]
        
        hexButtonArray.replaceObjectAtIndex(activeNodeIndex, withObject: dict)
        
        tagLastButton += 1
        
        let currentIndex = Int(activeButton.frame.origin.x / 72)
        
        if(currentIndex>=maxNodeIndex)
        {
            maxNodeIndex += 1
        }
        
        print("--#-- \(currentIndex) --- \(maxNodeIndex)")
        
        viewOptionsView.hidden = true
        
        print(hexButtonArray)
        
        let sizeViewBG = viewScrollViewBG.frame.size
        if(sizeViewBG.width < activeButton.center.x + 150)
        {
            viewScrollViewBGWidthConstraint.constant += 72 * 2
            self.viewScrollViewBG.layoutIfNeeded()
        }
    }
    
    @IBAction func btn1optionViewAction(sender: UIButton)
    {
        if(activeInsertButton != nil)
        {
            self.InsertNode(1)
        }
        else
        {
            SelectActiveButtonAsForward(1, forButton: sender)
        }
    }
    
    @IBAction func btn2optionViewAction(sender: UIButton)
    {
        if(activeInsertButton != nil)
        {
            self.InsertNode(2)
        }
        else
        {
            SelectActiveButtonAsForward(2, forButton: sender)
        }
    }
    
    @IBAction func btn3optionViewAction(sender: UIButton)
    {
        var image = sender.imageView?.image
        
        if(image == nil)
        {
            image = UIImage(named: "Repeat.png")
        }
        
        activeButton.setImage(image, forState: UIControlState.Normal)
        
        let dict = ["\(activeButton.tag)":
            ["Value":"3",
                "address":activeButton]]
        
        hexButtonArray.replaceObjectAtIndex(activeNodeIndex, withObject: dict)
        
        activeNodeIndex += 1
        maxNodeIndex += 1
        
        viewOptionsView.hidden = true
        
        shouldLeaveExtraSpace = 1
        
        if(activeNodeIndex < hexButtonArray.count)
        {
            let dictNextActiveButton = hexButtonArray.objectAtIndex(activeNodeIndex) as! NSDictionary
            
            let key = (dictNextActiveButton.allKeys as NSArray).objectAtIndex(0) as! String
            
            let btnActive = dictNextActiveButton.objectForKey(key)!.objectForKey("address")
            btnActive!.setImage(UIImage(named: "ActiveNode.png"), forState: UIControlState.Normal)
        }
        
        if(hasLeftArrow && arrArrowStack.count > 0)
        {
            arrArrowStack.removeLastObject()
        }
        hasLeftArrow = true
    }
    
    @IBAction func btn4optionViewAction(sender: UIButton)
    {
        if(activeInsertButton != nil)
        {
            self.InsertNode(4)
        }
        else
        {
            self.SelctActiveButtonAsFork(4, forButton: sender)
        }
    }
    
    @IBAction func btn5optionViewAction(sender: UIButton)
    {
        if(activeInsertButton != nil)
        {
            self.InsertNode(5)
        }
        else
        {
            self.SelctActiveButtonAsFork(5, forButton: sender)
        }
    }
    
    func SelctActiveButtonAsFork(type: Int, forButton sender: UIButton)
    {
        let currentIndex = Int(activeButton.frame.origin.x / 72)
        
        let tabIndex = maxNodeIndex - currentIndex
        
        print("------------\(tabIndex)")
        
        if(tabIndex >= 0)
        {
            var frame = activeButton.frame
            frame.origin.x += CGFloat((tabIndex + shouldLeaveExtraSpace) * 72)
            activeButton.frame = frame
            
            maxNodeIndex += shouldLeaveExtraSpace
            
            if((tabIndex + shouldLeaveExtraSpace) > 0 && arrArrowStack.count > 0)
            {
                activeArrow = arrArrowStack.lastObject as! UIImageView
                
                var arrowFrame = activeArrow.frame
                arrowFrame.size.width += CGFloat(tabIndex + shouldLeaveExtraSpace) * 72
                arrowFrame.size.height = 15
                activeArrow.frame = arrowFrame
                
                activeArrow.image = activeArrow.image!.resizableImageWithCapInsets(UIEdgeInsetsMake(5, 7, 7, 14))
                
                //                arrArrowStack.removeLastObject()
            }
            
            shouldLeaveExtraSpace = 0
        }
        
        activeButton.setImage(UIImage(named: "\(type).png"), forState: UIControlState.Normal)
        
        
        
        var dict = ["\(activeButton.tag)":
            ["Value":"\(type)",
                "address":activeButton]]
        
        hexButtonArray.replaceObjectAtIndex(activeNodeIndex, withObject: dict)
        activeNodeIndex += 1
        
        
        //------------------Second Button---------------Forward------------------
        
        var arrow = self.CreateArrowAtPoint(CGPointMake(activeButton.center.x + 72 - 30, activeButton.center.y))
        arrow.tag = tagLastArrow
        arrow.image = UIImage(named: "\(type)_R.png")
        viewScrollViewBG.addSubview(arrow)
        
        if(hasLeftArrow && arrArrowStack.count > 0)
        {
            arrArrowStack.replaceObjectAtIndex(arrArrowStack.count - 1, withObject: arrow)
        }
        else
        {
            arrArrowStack.addObject(arrow)
        }
        
        var buttonCenter = CGPointMake(activeButton.center.x + 72, activeButton.center.y)
        
        var button: UIButton = self.CreateNewButton(buttonCenter)
        button.setImage(UIImage(named: "InActiveNode.png"), forState: UIControlState.Normal)
        button.tag = tagLastButton
        viewScrollViewBG.addSubview(button)
        
        dict = ["\(tagLastButton)":
            ["Value":"-1",
                "address":button]]
        
        hexButtonArray.insertObject(dict, atIndex: activeNodeIndex)
        
        tagLastButton += 1
        
        //------------------Third Button------------Down---------------------
        
        arrow = self.CreateDownArrowAtPoint(CGPointMake(activeButton.center.x, activeButton.center.y + 124 - 78))
        arrow.tag = tagLastArrow
        
        var arrowFrame = arrow.frame
        arrowFrame.size.width = 15
        arrowFrame.size.height = 64
        arrow.frame = arrowFrame
        
        arrow.image = UIImage(named: "\(type)_D.png")?.resizableImageWithCapInsets(UIEdgeInsetsMake(7, 5, 14, 7))
        
        viewScrollViewBG.addSubview(arrow)
        
        
        
        buttonCenter = CGPointMake(activeButton.center.x, activeButton.center.y + 124)
        
        button = self.CreateNewButton(buttonCenter)
        button.tag = tagLastButton
        viewScrollViewBG.addSubview(button)
        
        dict = ["\(tagLastButton)":
            ["Value":"-1",
                "address":button]]
        
        hexButtonArray.insertObject(dict, atIndex: activeNodeIndex)
        
        tagLastButton += 1
        
        viewOptionsView.hidden = true
        
        hasLeftArrow = false
        
        let sizeViewBG = viewScrollViewBG.frame.size
        if(sizeViewBG.width < activeButton.center.x + 150)
        {
            viewScrollViewBGWidthConstraint.constant += 72 * 2
            self.viewScrollViewBG.layoutIfNeeded()
        }
        
        if(sizeViewBG.height < activeButton.center.y + 150)
        {
            viewScrollViewBGHeightConstraint.constant += 124 * 2
            self.viewScrollViewBG.layoutIfNeeded()
        }
    }
    
    @IBAction func btnMiddleOptionViewAction(sender: UIButton)
    {
        viewOptionsView.hidden = true
        viewOptionInsertDelete.hidden = true
    }
    
    func RefreshFlowUI(arrFlow: NSArray)
    {
        print("array - \(arrFlow)")
        
        //        var count = 0
        //        maxNodeIndex = 2
        self.InitialiseNodes()
        
        //        var activePoint = point
        
        for i in 1 ..< arrFlow.count
        {
            let dict = arrFlow.objectAtIndex(i) as! NSDictionary
            
            let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
            
            //            let button = dict.objectForKey(key)!.objectForKey("address") as! UIButton
            
            let value = Int(dict.objectForKey(key)!.objectForKey("Value") as! String)
            
            if(value == -1)
            {
                break
            }
            
            if(value <= 2)
            {
                let dict = hexButtonArray.objectAtIndex(activeNodeIndex) as! NSDictionary
                
                let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
                
                let button = dict.objectForKey(key)!.objectForKey("address") as! UIButton
                
                activeButton = button
                SelectActiveButtonAsForward(value!, forButton: button)
                
                //                button.center = activePoint
                //                activePoint.x += 72
                
            }
            else if(value == 3)
            {
                let dict = hexButtonArray.objectAtIndex(activeNodeIndex) as! NSDictionary
                
                let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
                
                let button = dict.objectForKey(key)!.objectForKey("address") as! UIButton
                
                activeButton = button
                btn3optionViewAction(button)
            }
            else
            {
                let dict = hexButtonArray.objectAtIndex(activeNodeIndex) as! NSDictionary
                
                let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
                
                let button = dict.objectForKey(key)!.objectForKey("address") as! UIButton
                
                activeButton = button
                SelctActiveButtonAsFork(value!, forButton: button)
            }
            
        }
    }
    
    @IBAction func HandleDeleteButton(sender: AnyObject)
    {
        viewOptionInsertDelete.hidden = true
        for i in 0..<hexButtonArray.count
        {
            let dict = hexButtonArray.objectAtIndex(i) as! NSDictionary
            
            let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
            
            if(activeDeleteButton.tag == Int(key))
            {
                //                hexButtonArray.removeObjectAtIndex(i)
                
                var endIndex = i
                var count = 0
                for j in (i+1)..<hexButtonArray.count
                {
                    if(count<0)
                    {
                        endIndex = j-1
                        break
                    }
                    
                    let dict = hexButtonArray.objectAtIndex(j) as! NSDictionary
                    
                    let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
                    
                    let value = Int(dict.objectForKey(key)!.objectForKey("Value") as! String)
                    
                    if(value>3)
                    {
                        count += 1
                    }
                    else if(value == 3)
                    {
                        count -= 1
                    }
                    
                }
                
                let myRange = i...endIndex
                
                hexButtonArray.removeObjectsInRange(NSRange(myRange))
                
                self.RefreshFlowUI(hexButtonArray.copy() as! NSArray)
                break
            }
        }
    }
    
    @IBAction func HandleInsertButton(sender: AnyObject)
    {
        viewOptionInsertDelete.hidden = true
        for i in 0..<hexButtonArray.count
        {
            let dict = hexButtonArray.objectAtIndex(i) as! NSDictionary
            
            let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
            
            if(activeDeleteButton.tag == Int(key))
            {
                let button = dict.objectForKey(key)!.objectForKey("address") as! UIButton
                activeButton = button
                
                let centerFrame = activeButton.center
                //                centerFrame.x += 15
                viewOptionsView.center = centerFrame
                viewOptionsView.hidden = false
                btn3OptionsView.hidden = true
            }
        }
    }
    
    func InsertNode(type: Int)
    {
        for i in 0..<hexButtonArray.count
        {
            let dict = hexButtonArray.objectAtIndex(i) as! NSDictionary
            
            let key = (dict.allKeys as NSArray).objectAtIndex(0) as! String
            
            if(activeInsertButton.tag == Int(key))
            {
                var dict = ["\(tagLastButton)":["Value":"\(type)","address":activeButton]]
                
                hexButtonArray.insertObject(dict, atIndex: i)
                
                if(type>3)
                {
                    dict = ["\(tagLastButton)":["Value":"\(3)","address":activeButton]]
                    
                    hexButtonArray.insertObject(dict, atIndex: i+1)
                }
                self.RefreshFlowUI(hexButtonArray.copy() as! NSArray)
                break
            }
        }
    }
    
    @IBAction func HandleRefresh(sender: AnyObject)
    {
        //        self.InitialiseNodes()
        self.RefreshFlowUI(hexButtonArray.copy() as! NSArray)
    }
    
    @IBAction func HandleBack(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func createComponentsArray(id: Int)
    {
        switch id {
        case 1:
            settingView.arrComponents.removeAll()
            settingView.arrComponents = [6,7,15,8,6]
            
        case 2:
            settingView.arrComponents.removeAll()
            settingView.arrComponents = [1,2,3,4]
        
        case 3:
            settingView.arrComponents.removeAll()
            
        case 4:
            settingView.arrComponents.removeAll()
            settingView.arrComponents = [16,17]
            
        case 5:
            settingView.arrComponents.removeAll()
            settingView.arrComponents = [0]
            
        default:
            settingView.arrComponents.removeAll()
            settingView.arrComponents = [18]
        }
        settingView.tableView.reloadData()
    }
    
}
